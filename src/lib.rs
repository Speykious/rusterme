#[macro_use]
extern crate lazy_static;

use roxmltree::{Document, NodeType, Node};
use regex::Regex;
use std::io::{self, BufReader, BufRead, Read};
use std::collections::{HashMap, HashSet};
use ansi_term::Style;

pub mod duml;
pub mod utils;
pub mod print_utils;

use utils::push_inside;
use duml::{DumConfig, SimpleDumConfig};

lazy_static! {
  static ref MAPFILE_RE: Regex = Regex::new(r"^(\w+)\s+(\w+)$").unwrap();
  static ref MARS_RE: Regex = Regex::new(r"^\[MARS_ATS_SsyAD_\d+\]$").unwrap();
  static ref HMI_RE: Regex = Regex::new(r"^.*HMI shall (read( .+ on the following plug :| the( plug)?)|use the( plugs?)?|write \d on( plug)?|get the list of application object names from .+ object and set to \d all) (\w*)<(\w+)>((\.\w+)+).*$").unwrap(); // Thicc
  static ref HMI_GLOBAL_RE: Regex = Regex::new(r"HMI shall write in the plug (\w+(\.\w+)+) the value .*").unwrap();
  static ref PARAM_TYPE_RE: Regex = Regex::new(r"^(\w+):$").unwrap();
  static ref PARAM_PLUG_RE: Regex = Regex::new(r"^\| (\w*%?(\.\w+)+)$").unwrap();
  pub static ref PLUG_RE: Regex = Regex::new(r"^(\w*)(%?)(([.;]\w+)+)$").unwrap();
  static ref PARAM_COMM_RE: Regex = Regex::new(r"^\| # (.+)$").unwrap();
  static ref BOLD: Style = Style::new().bold();
}

fn tag_string(node: &Node) -> String {
  format!("{:?}", node.tag_name()).to_string()
}

/// Gathers up ATS-instances from the document into a hashmap.
/// The keys correspond to each existing tag, and the value vectors contain each instance's value of the Name attribute for a given key.
pub fn read_xml_instances(doc: &Document) -> SimpleDumConfig {
  let mut instances = HashMap::new();

  for node in doc.descendants() {
    if let (NodeType::Element, Some(name)) = (node.node_type(), node.attribute("Name")) {
      push_inside(&mut instances, tag_string(&node), name.to_string());
    }
  }

  instances
}

/// Gathers up ATS-instances from the csv file into a hashmap.
/// The keys correspond to each existing tag, and the value vectors contain each instance's value of the Name attribute for a given key.
pub fn read_csv_instances<R: Read>(readable: R) -> io::Result<SimpleDumConfig> {
  let mut hashmap = HashMap::new();

  for line in BufReader::new(readable).lines() {
    let line = line?;
    let mut csv_split = line.split(';');
    let ats_type = csv_split.nth(1).unwrap().to_string();
    if ats_type == "Equipment_Type" { continue; }
    let ats_instance = csv_split.nth(6).unwrap().to_string();
    if ats_instance == "" { continue; } // I noticed incomplete CSV lines in the file, so I ignored them
    push_inside(&mut hashmap, ats_type, ats_instance);
  }

  Ok(hashmap)
}

/// Gathers up ATS-parameters from the csv file into a hashmap.
pub fn read_csv_parameters<R: Read>(readable: R) -> io::Result<SimpleDumConfig> {
  let mut hashmap = HashMap::new();

  for line in BufReader::new(readable).lines() {
    let line = line?;
    let mut csv_split = line.split(';');
    if let (Some(csv_key), Some(csv_param)) = (csv_split.next(), csv_split.next()) {
      let (csv_key, csv_param) = (csv_key.to_string(), csv_param.to_string());
      /*
      // CSV file generated without part before underscore for now
      let under_offset = csv_param.find('_').unwrap_or(csv_param.len() - 1) + 1;
      let csv_param = csv_param.split_off(under_offset);
      */
      push_inside(&mut hashmap, csv_key.clone(), csv_param);
    }
  }

  Ok(hashmap)
}

/// Old version taking an XML file as input.
/// The keys correspond to each existing tag's value of the Name attribute, and the value vectors contain each parameter's value of the Tag attribute for a given key.
pub fn read_xml_parameters(doc: &Document) -> SimpleDumConfig {
  let mut parameters = HashMap::new();

  if let Some(first_child) = doc.root().first_child() {
    for node in first_child.children().filter(Node::is_element) {
      if let (Some(tags_node), Some(name)) = (node.first_element_child(), node.attribute("Name")) {
        let key = name.to_string();
        parameters.insert(key.clone(), HashSet::new());
        let set = parameters.get_mut(&key).unwrap(); // We just created it on the line before, duh
        for sub_node in tags_node.children().filter(Node::is_element) {
          // Truncating everything before the first occurrence of `_`
          let mut full_tag = sub_node.attribute("Tag").unwrap().to_string();
          let under_offset = full_tag.find('_').unwrap_or(full_tag.len() - 1) + 1;
          let tag = full_tag.split_off(under_offset);
          set.insert(tag);
        }
      }
    }
  }

  parameters
}

pub fn read_relevant_human_lines<R: Read>(readable: R) -> io::Result<Vec<String>> {
  let mut relevant_lines = Vec::new();

  let reader = BufReader::new(readable);
  let mut lines = reader.lines();
  while let Some(line) = lines.next() {
    let line = line?;
    if MARS_RE.is_match(&line) {
      if let Some(next_line) = lines.next() {
        let next_line = next_line?;
        relevant_lines.push(next_line);
      }
    }
  }

  Ok(relevant_lines)
}

/// Requirements changed (again!), we now have to read some human :v
pub fn read_human_parameters<R: Read>(readable: R) -> io::Result<DumConfig> {
  let mut out_config: DumConfig = HashMap::new();

  let relevant_lines = read_relevant_human_lines(readable)?;
  for sentence in relevant_lines {
    let (comment, ats_type, ats_param) = {
      if let Some(cap) = HMI_RE.captures(&sentence) {
        (cap[0].to_string(), cap[7].to_string(), format!("{}%{}", cap[6].to_string(), cap[8].to_string()))
      } else if let Some(cap) = HMI_GLOBAL_RE.captures(&sentence) {
        (cap[0].to_string(), "__global__".to_string(), cap[1].to_string())
      } else { continue; }
    };

    if let Some(mut out_params) = out_config.get_mut(&ats_type) {
      push_inside(&mut out_params, ats_param, comment);
    } else {
      let mut out_params = HashMap::new();
      push_inside(&mut out_params, ats_param, comment);
      out_config.insert(ats_type, out_params);
    }
  }

  Ok(out_config)
}

pub fn read_map<R: Read>(readable: R) -> io::Result<HashMap<String, String>> {
  let reader = BufReader::new(readable);
  let mut hashmap = HashMap::new();
  for line in reader.lines() {
    let line = line?;
    if let Some(caps) = MAPFILE_RE.captures(&line) {
      hashmap.insert(caps[1].to_string(), caps[2].to_string());
    }
  }

  Ok(hashmap)
}
