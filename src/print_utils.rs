use std::io;
use roxmltree::{Document, NodeType, Node};
use ansi_term::{Color::{Red, Cyan, Green, Yellow}, Style};
use crate::duml::{DumConfig, SimpleDumConfig, write_dum_config, write_simple_dum_config};

////////////////////////////////////////////////////////
// Printing utilities: seeing what we're dealing with //
////////////////////////////////////////////////////////

/// Print an element tag with all its attributes.
pub fn print_element_tag(node: &Node) {
  let bold = Style::new().bold();
  print!("{}", Red.bold().paint(format!("{:?}", node.tag_name())));
  for attr in node.attributes() {
    print!(
      " {}={}",
      bold.paint(attr.name()),
      Cyan.paint(format!("\"{}\"", attr.value())),
    );
  }
  println!();
}

/// Print all ATS-instances from a document.
/// It is assumed that it is the document actually containing the instances.
pub fn print_xml_instances(doc: &Document) {
  for node in doc.descendants() {
    match node.node_type() {
      NodeType::Element => {
        match node.attribute("Name") {
          Some(_name) => print_element_tag(&node),
          None => (),
        }
      },
      NodeType::Root =>
        println!("<==> <{}> <==>", Red.bold().paint("Root")),
      _ => (),
    }
  }
}

/// Print all ATS-parameters from a document.
/// It is assumed that it is the document actually containing the parameters.
pub fn print_xml_parameters(doc: &Document) {
  let bold = Style::new().bold();
  let dim = Style::new().dimmed();

  if let Some(first_child) = doc.root().first_child() {
    for node in first_child.children().filter(Node::is_element) {
      print_element_tag(&node);
      if let Some(tags_node) = node.first_element_child() {
        for sub_node in tags_node.children().filter(Node::is_element) {
          print!("{}", dim.paint("| "));
          println!("{} {}",
            bold.paint("Tag"),
            Green.paint(sub_node.attribute("Tag").unwrap()),
          );
        }
      }
    }
  }
}

pub fn print_dum_config(dum_config: &DumConfig) -> io::Result<()> {
  write_dum_config(&mut std::io::stdout(), dum_config)
}

pub fn print_simple_dum_config(simple_dum_config: &SimpleDumConfig) -> io::Result<()> {
  write_simple_dum_config(&mut std::io::stdout(), simple_dum_config)
}

pub fn print_ok() {
  println!("{}", Yellow.bold().paint("OK"));
}
