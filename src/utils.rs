use std::collections::{HashMap, HashSet};
use std::fmt::Display;
use std::hash::Hash;
use ansi_term::Colour::Red;
use roxmltree::Document;

/// Push a value to the end of a vector inside a hashmap.
pub fn push_inside<K: Eq + Hash, V: Eq + Hash>(hashmap: &mut HashMap<K, HashSet<V>>, key: K, value: V) {
  if let Some(set) = hashmap.get_mut(&key) {
    set.insert(value);
  } else {
    hashmap.insert(key, {
      let mut set = HashSet::new();
      set.insert(value);
      set
    });
  }
}

/// Dumb titlecase function: "hello world" to "Hello world"
/// Only uppercases the first character in the String
pub fn make_ascii_titlecase(s: &str) -> String {
  let mut s: String = s.to_string();
  if let Some(r) = s.get_mut(0..1) {
    r.make_ascii_uppercase();
  }
  return s;
}

/// Dumb titlecasen't function: "HELLO WORLD" to "hELLO WORLD"
/// Only lowercases the first character in the String
pub fn make_ascii_dumbcase(s: &str) -> String {
  let mut s = s.to_string();
  if let Some(r) = s.get_mut(0..1) {
    r.make_ascii_lowercase();
  }
  return s;
}

//////////////////////////////////////////////////////////////
// Shell interaction utilities: error messages, early exits //
//////////////////////////////////////////////////////////////

/// Print a generic error message with a prepending "Error:" string in bold red.
pub fn eprint<D: Display>(error: D) {
  eprintln!("{} {}", Red.bold().paint("Error:"), error);
}

/// Print a generic error message and die.
pub fn eprint_die<D: Display>(error: D) -> ! {
  eprint(error);
  std::process::exit(1);
}

/// Retrieve the XML document or die.
pub fn doc_or_die(content: &str) -> Document {
  match Document::parse(&content) {
    Ok(doc) => doc,
    Err(e) => eprint_die(e),
  }
}

