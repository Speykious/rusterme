use std::fs::File;
use common::*;
use crate::print_utils::print_dum_config;
use getopts::Options;

fn usagexit(arg0: &str, opts: Options, exit_code: i32) -> ! {
  let brief = format!("Usage: \x1b[1m{}\x1b[0m <parameter-file> [options]", arg0);
  print!("{}", opts.usage(&brief));
  std::process::exit(exit_code);
}

fn main() -> std::io::Result<()> {
  let args: Vec<_> = std::env::args().collect();

  // Argument parsing
  let mut opts = Options::new();
  opts.optflag("h", "help", "Print this help menu.");
  opts.optflag("l", "lines", "Print relevant lines from the parameter file.");
  let matches = match opts.parse(&args[1..]) {
    Ok(m) => m,
    Err(f) => panic!("{}", f.to_string()),
  };
  if matches.opt_present("h") {
    usagexit(&args[0], opts, 0);
  }
  let optl = matches.opt_present("l");
  let params_filename = if !matches.free.is_empty() {
    matches.free[0].clone()
  } else {
    eprintln!("Error: parameter file is not defined.");
    usagexit(&args[0], opts, 1);
  };
  
  if optl {
    println!("Relevant lines:");
    for line in read_relevant_human_lines(File::open(params_filename)?)? {
      println!("{}", line);
    }
  } else {
    let parameters = read_human_parameters(File::open(args[1].to_string())?)?;
    print_dum_config(&parameters)?;
  }

  Ok(())
}
