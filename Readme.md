# Rusterme

> Programme pour générer des fichiers termes pour NEOMMA à partir d'un fichier d'instances ATS, un fichier de paramètres ATS et un fichier de correspondances de types d'instances ATS.

## Post-installation

Avant d'installer Rust, il faut avoir au préalable installé les **Visual Studio C++ Build Tools**. Si Visual Studio 2015, 2017, 2019 ou plus récent est installé sur la machine, il suffit d'avoir à disposition les outils de développement C++ pour applications desktop.

## Installer Rust

1. Se rendre vers https://www.rust-lang.org/tools/install puis cliquer sur **DOWNLOAD RUSTUP-INIT.EXE (64-BIT)**.
   ![install-rust:rustup-init](./rust-tutorial-screenshots/install-rust-0_rustup.png)

   
   
2. Un exécutable nommé `rustup-init.exe` atterrira dans votre dossier de téléchargements. Cliquer dessus pour l'exécuter.
   ![install-rust:exe](./rust-tutorial-screenshots/install-rust-1_exe.png)

   

3. Une console Windows s'ouvrira alors avec des informations sur l'installation de Rust.
   Si vous voyez des avertissements à propos des C++ Build Tools, référez-vous à la post-installation.
   Si vous voyez des informations similaires à la capture d'écran ci-dessous, appuyer sur la touche **Entrée**.
   ![install-rust:cli-start](./rust-tutorial-screenshots/install-rust-2_installation.png)

   

4. L'exécutable va maintenant installer les outils Rust, tels que Cargo, Clippy et Rustc.
   Une fois l'installation terminée, vous devriez voir des informations similaires à la capture d'écran ci-dessous : appuyer alors sur la touche **Entrée**.
   ![install-rust:cli-end](./rust-tutorial-screenshots/install-rust-3_termine.png)

Rust est maintenant installé sur le système.

## Compiler Rusterme

Pour compiler Rusterme, il faut se rendre dans le terminal. Les instructions suivantes utilisent **Windows Powershell**, mais il est tout aussi possible de compiler avec **cmd** ou **Git Bash**.

1. Ouvrir l'application **Windows Powershell**.

2. Taper `cd ` dans la ligne de commande (sans taper sur Entrée), puis prendre et glisser le dossier `rusterme` depuis l'explorateur de fichiers vers la fenêtre du Windows Powershell, et enfin taper sur **Entrée**.

3. Taper `cargo build --release --bin rusterme` dans la ligne de commande pour compiler l'exécutable `rusterme`.
   ![compilation:cargo](./rust-tutorial-screenshots/compilation-0_cargo.png)

   

   L'exécutable se trouvera dans le sous-dossier `rusterme\target\release\` et se nommera `rusterme.exe`.
   ![compilation:exe](./rust-tutorial-screenshots/compilation-1_exe.png)

   

   Cependant, il n'est pas nécessaire d'appeler directement l'exécutable : le package manager Cargo possède le verbe `run` permettant de le faire automatiquement depuis le dossier racine du projet, ici `rusterme\`.

## Utiliser Rusterme

1. Pour lancer l'exécutable `rusterme`, on peut donc écrire `cargo run --release --bin rusterme`.
   Dans le terminal, il devrait alors s'afficher le message suivant :
   ```sh
   Usage: ./target/release/rusterme.exe <instances.csv> <parameters.txt> <map.txt>
   ```
   Il décrit l'usage de l'exécutable. Il faut d'abord lui fournir le fichier d'instances au format `csv` (encodage `utf-8`, séparé par des `;`), puis le fichier des paramètres converti en `txt` depuis le document Word correspondant, puis enfin le fichier de correspondances de types entre les deux fichiers.

   
   
2. **Exemple d'utilisation :**
   ```sh
   cargo run --release --bin rusterme <instances.csv> <parameters.txt> <map.txt>
   ```
   ![exemple-utilisation](./rust-tutorial-screenshots/generation-terme-0_cargo-run.png)

   
   
   À partir du fichier d'instance `.\in\ats-system-instances-v3.csv`, du fichier de paramètres `.\in\ats-params.txt` et du fichier de correspondances de types `.\in\ats-type-map.txt`, l'exécutable `rusterme.exe` générera un fichier terme dans `out/terme.txt`.
   Il détaillera aussi diverses informations telles que les types d'instances présents dans le fichier d'instances, les types d'instances présents dans le fichier des paramètres, et la structure obtenue du fichier de correspondances de types.
   ![out:terme.txt](./rust-tutorial-screenshots/generation-terme-1_out-terme.png)
   > Note : le fichier de paramètres est différent sur la capture d'écran car il y a eu un changement de code entre temps.

Le fichier terme est maintenant généré.
