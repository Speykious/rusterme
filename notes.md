# Notes et questions : Création d'un générateur de fichiers terme

## Description des fichiers
- `ats-system-instances.xml` : liste des instances de tous les objets sur le terrain.
  *Exemple pour des aiguilles:*
  
  ```xml
  <Point ID="1" Name="AIG_16A_LRM_M1">...</Point>
  <Point ID="2" Name="AIG_36A_LRM_M1">...</Point>
  <Point ID="3" Name="AIG_15_LRM_M1">...</Point>
  <Point ID="4" Name="AIG_36B_LRM_M1">...</Point>
  ```
- `ats-system-parameters.xml` : liste des informations remontées par le terrain pour chaque type d'objet.
  *Exemple pour des aiguilles:*
  
  ```xml
  <Point_Tag ID="3" Name="Point">
    <Tag ID="1" Name="SwitchState" Tag="PointEnd_Detection.Template.iEqpState" .../>
    <Tag ID="2" Name="SwitchMode" Tag="PointEnd_Mode.Template.iEqpState" .../>
  </Point_Tag>
  ```

## Questions

- Est-ce que la table de correspondance sera une association entre le `tag` d'une ATS-instance et la valeur de l'attribut `Name` d'un ATS-paramètre ?

  ​	Exemple : `Point <--> Point`, `Secondary_Detection_Device <--> SDD`...

- Ou alors, est-ce que ce sera entre le début (avant `_`) de la valeur de l'attribut `Name` d'une ATS-instance (exemple : `AIG` dans `AIG_16A_LRM_M1`) et la valeur de l'attribut `Name` d'un ATS-paramètre ?

  ​	Exemple : `AIG <--> Point`, `CDV <--> SDD`...
