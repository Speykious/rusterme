use std::collections::{HashMap, HashSet};
use std::io::{self, BufReader, BufRead, Read, Write};
use regex::Regex;
use crate::utils::push_inside;

lazy_static! {
  static ref DUML_KEY_RE: Regex = Regex::new(r"^(\w+):$").unwrap();
  static ref DUML_FIELD_RE: Regex = Regex::new(r"^\| (.+)$").unwrap();
  static ref DUML_COMM_RE: Regex = Regex::new(r"^\| # (.+)$").unwrap();
}

/// Represents a single field in a DUML file, along with its comments.
pub type DumField = HashMap<String, HashSet<String>>;
/// Represents a complete DUML config, with all keys and all their respective fields.
pub type DumConfig = HashMap<String, DumField>;

/// Represents a single field in a DUML file, without comments.
pub type SimpleDumField = HashSet<String>;
/// Represents a complete DUML config (without comments), with all keys and all their respective fields.
pub type SimpleDumConfig = HashMap<String, SimpleDumField>;

/// Reads a DUML file into a DumConfig datatype.
pub fn read_dum_config<R: Read>(readable: R) -> io::Result<DumConfig> {
  let mut dum_config: DumConfig = HashMap::new();
  let (mut current_dum_key, mut current_dum_field) = (String::new(), String::new());
  
  for line in BufReader::new(readable).lines() {
    let line = line?;
    if let Some(cap) = DUML_KEY_RE.captures(&line) {
      let dum_key = cap[1].to_string();
      current_dum_key = dum_key.clone();
    } else if let Some(cap) = DUML_FIELD_RE.captures(&line) {
      let dum_field = cap[1].to_string();
      current_dum_field = dum_field.clone();
    } else if let Some(cap) = DUML_COMM_RE.captures(&line) {
      let (dum_comment, dum_key, dum_field) = (cap[1].to_string(), current_dum_key.clone(), current_dum_field.clone());
      if let Some(mut dum_fields) = dum_config.get_mut(&dum_key) {
        push_inside(&mut dum_fields, dum_field, dum_comment);
      } else {
        let mut dum_fields = HashMap::new();
        push_inside(&mut dum_fields, dum_field, dum_comment);
        dum_config.insert(dum_key, dum_fields);
      }
    }
  }

  Ok(dum_config)
}

/// Writes a DumConfig datatype into a DUML file.
pub fn write_dum_config<W: Write>(writeable: &mut W, dum_config: &DumConfig) -> io::Result<()> {
  for (dum_key, dum_fields) in dum_config {
    writeln!(writeable, "{}:", dum_key)?;
    for (dum_field, dum_comments) in dum_fields {
      writeln!(writeable, "| {}", dum_field)?;
      for dum_comment in dum_comments {
        writeln!(writeable, "| # {}", dum_comment)?;
      }
      writeln!(writeable, "|")?;
    }
  }

  Ok(())
}

/// Reads a DUML file into a SimpleDumConfig datatype.
pub fn read_simple_dum_config<R: Read>(readable: R) -> io::Result<SimpleDumConfig> {
  let mut simple_dum_config: SimpleDumConfig = HashMap::new();
  let mut current_dum_key = String::new();
  
  for line in BufReader::new(readable).lines() {
    let line = line?;
    if let Some(cap) = DUML_KEY_RE.captures(&line) {
      let dum_key = cap[1].to_string();
      current_dum_key = dum_key.clone();
    } else if DUML_COMM_RE.is_match(&line) {
        continue;
    } else if let Some(cap) = DUML_FIELD_RE.captures(&line) {
      let dum_field = cap[1].to_string();
      let dum_key = current_dum_key.clone();
      push_inside(&mut simple_dum_config, dum_key, dum_field);
    }
  }

  Ok(simple_dum_config)
}

/// Writes a SimpleDumConfig datatype into a DUML file.
pub fn write_simple_dum_config<W: Write>(writeable: &mut W, dum_config: &SimpleDumConfig) -> io::Result<()> {
  for (dum_key, dum_fields) in dum_config {
    writeln!(writeable, "{}:", dum_key)?;
    for dum_field in dum_fields {
      writeln!(writeable, "| {}", dum_field)?;
    }
  }

  Ok(())
}

