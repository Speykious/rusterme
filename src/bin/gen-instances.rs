use std::fs::File;
use common::*;
use crate::print_utils::print_simple_dum_config;
use crate::utils::{doc_or_die, push_inside};
use getopts::Options;

fn usagexit(arg0: &str, opts: Options, exit_code: i32) -> ! {
  let brief = format!("Usage: \x1b[1m{}\x1b[0m <instances.xml> <instances.csv> [options]", arg0);
  print!("{}", opts.usage(&brief));
  std::process::exit(exit_code);
}

fn main() -> std::io::Result<()> {
  let args: Vec<_> = std::env::args().collect();

  // Argument parsing
  let mut opts = Options::new();
  opts.optflag("h", "help", "Print this help menu.");
  
  let matches = match opts.parse(&args[1..]) {
    Ok(m) => m,
    Err(f) => panic!("{}", f.to_string()),
  };
  if matches.opt_present("h") {
    usagexit(&args[0], opts, 0);
  }

  if matches.free.len() != 2 {
    eprintln!("Error: wrong number of arguments.");
    usagexit(&args[0], opts, 1);
  }

  let xml_content = std::fs::read_to_string(&matches.free[0])?;
  let mut instances_dum_config = read_xml_instances(&doc_or_die(&xml_content));
  let csv_instances_dum_config = read_csv_instances(File::open(&matches.free[1])?)?;

  for (ats_type, ats_instances) in csv_instances_dum_config {
    for ats_instance in ats_instances {
      push_inside(&mut instances_dum_config, ats_type.clone(), ats_instance);
    }
  }

  print_simple_dum_config(&instances_dum_config)?;

  Ok(())
}
