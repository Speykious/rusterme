#[macro_use]
extern crate lazy_static;
use std::collections::HashMap;
use std::io::Write;
use std::fs::File;

use regex::Regex;
use ansi_term::Color::Green;
use common::{PLUG_RE, read_map};
use common::duml::{SimpleDumConfig, read_simple_dum_config};
use common::print_utils::print_ok;

lazy_static! {
  static ref FROTTEUR_RE: Regex = Regex::new(r"Frotteurn\.(.*)").unwrap();
}

////////////////////
// The Holy Grail //
////////////////////

/// Behold, the Holy Grail of my internship mission \o/
/// Generates a list of terms from an instances hashmap, parameters hashmap, and pi-correspondances hashmap.
fn generate_terms(
  instances: SimpleDumConfig,
  parameters: SimpleDumConfig,
  pi_map: HashMap<String, String>,
) -> Vec<String> {
  let mut terms = Vec::new();

  for (ats_type, ats_params) in parameters.iter() {
    let instance_tag = pi_map.get(ats_type).unwrap_or(ats_type);
    if instance_tag == "__global__" {
      for ats_param in ats_params {
        terms.push(ats_param.clone());
      }
    } else if let Some(ats_instances) = instances.get(instance_tag) {
      for ats_instance in ats_instances {
        for ats_param in ats_params {
          let (prefix, plug) = if let Some(cap) = PLUG_RE.captures(&ats_param) {
            (cap[1].to_string(), cap[3].to_string())
          } else { panic!("Nani -> {}", ats_param) };

          if let Some(cap) = FROTTEUR_RE.captures(&plug) {
            let after_dot = cap[1].to_string();
            for i in 1..=8 {
              terms.push(format!("{}{}.Frotteur{}.{}", prefix, ats_instance, i, after_dot));
            }
          } else {
            terms.push(format!("{}{}{}", prefix, ats_instance, plug));
          }
        }
      }
    }
  }

  terms.sort();
  terms
}

////////////////////////////////
// Entry point of the program //
////////////////////////////////

fn main() -> std::io::Result<()> {
  let args: Vec<_> = std::env::args().collect();
  if args.len() != 3 && args.len() != 4 {
    println!("\n  Usage: \x1b[1m{}\x1b[0m <instances.duml> <parameters.duml> [map.txt]\n", args[0]);
    std::process::exit(1);
  }

  let instances_filename = args[1].clone();
  let parameters_filename = args[2].clone();

  print!("Parsing instances in DUML... ");
  let instances = read_simple_dum_config(File::open(instances_filename)?)?;
  print_ok();
  print!("Parsing parameters in DUML... ");
  let parameters = read_simple_dum_config(File::open(parameters_filename)?)?;
  print_ok();
  print!("Parsing IP-map file... ");
  let pi_map = if args.len() == 4 { read_map(File::open(args[3].clone())?)? } else { HashMap::new() };
  print_ok();

  let mut instance_keys = instances.keys().collect::<Vec<_>>();
  instance_keys.sort();
  println!("\nTypes in instances folder: {:#?}", instance_keys);
  let mut parameter_keys = parameters.keys().collect::<Vec<_>>();
  parameter_keys.sort();
  println!("\nTypes in parameters folder: {:#?}", parameter_keys);
  println!("\nCorrespondance map: {:#?}", pi_map);

  // Actually generated terms
  let terms = generate_terms(instances, parameters, pi_map);
  // println!("\nGenerated terms: {:#?}", terms);
  
  let term_filename = "out/terme.txt";
  print!("Saving terms to {}... ", Green.paint(term_filename));
  let term_string = terms.join("\r\n");
  let mut term_file = File::create(term_filename)?;
  term_file.write_all(term_string.as_bytes())?;
  print_ok();

  Ok(())
}
